﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foo.Test
{
    public class Class1
    {
        [TestCaseSource(nameof(BadData))]
        public void test(TestData x)
        {
            Assert.That(x != null);
        }

        [TestCase("\n")]
        public void test2(string x)
        {
            Assert.That(x != null);
        }

        public class TestData
        {
            public string Data { get; set; }
            public override string ToString()
            {
                return Data;
            }
        }

        public static IEnumerable BadData
        {
            get
            {
                yield return new object[] { new TestData { Data = "\n" } };
                yield return new object[] { new TestData { Data = " " } };
            }
        }
    }
}
